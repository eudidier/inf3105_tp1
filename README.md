# README #

La direction d’une commission scolaire incluant plusieurs écoles primaires a décidé, avant d’assigner les élèves aux écoles, d’expérimenter un système d’assignation automatique selon un critère de distance minimale entre la maison d’un élève et son école.
La direction vous a fourni le nom des élèves avec les adresses correspondantes aux coordonnées géographiques exprimées sous forme de latitudes et longitudes. En connaissant les coordonnées des écoles, écrivez un programme C++ nommé tp1 qui, selon les distances calculées et le nombre de places disponibles, assignera les élèves à un de leur école. Affichez ces assignations par école avec la capacité de l’école ainsi que le nombre d’élèves inscrits. Les capacités, les coordonnées et les noms des écoles sont fournis.
Les données doivent être emmagasinées à l'aide de la structure de données Tableau (voir fichier tableau.h), telle que décrite dans les notes de cours. Vous ne pouvez pas utiliser les structures de données de la librairie standard de C++ (ex.: std::vector ou std::list).
3. Structure du programme
Un squelette de départ est disponible dans tp1.zip.
 Ce squelette vous est fourni pour vous aider à vous concentrer sur l'essentiel du
TP1.
 Vous êtes libres de l'utiliser ou non.
 Vous pouvez y effectuer toutes les modifications que vous souhaitez ou jugez
nécessaires.
 Ce squelette vise d'abord la simplicité pour obtenir rapidement un programme
fonctionnel.
  
3.1 Syntaxe d'appel du programme tp1
Votre programme doit pouvoir être lancé en ligne de commande avec la syntaxe suivante :
./tp1 listeEleves.txt listeEcoles.txt
Où listeEleves.txt et listeEcoles.txt sont les noms des fichiers contenant respectivement les listes des élèves et des écoles avec les adresses sous forme de coordonnées.
Votre programme doit lire les fichiers au moyen d'un flux de lecture C++ std::ifstream.
3.2 Entrées
a) Format d'un fichier de la liste des élèves Un fichier est formé de la manière suivante.
 le nom et le prénom sur une ligne;
 la coordonnée dans les parenthèses;
 un point-virgule (;)
Le nombre d’élèves est variable. Voici un exemple d'entrée ( listeEleves 00.txt).
b) Format d'un fichier de la liste des écoles
 le nom de l’école suivie de la capacité de cette école sur une ligne;
 la coordonnée dans les parenthèses;
Le nombre d’écoles est variable. Voici un exemple d'entrée ( listeEcoles .txt).
               NOM1 PRENOM1
(Lattitude1, Longitude1);
NOM2 PRENOM2
(Lattitude2, Longitude2);
NOM3 PRENOM3
...
                   ANGELINA Lécuyer
(45.4372,-73.8609);
CHARLES Campbell
(45.5092,-73.5680);
               NOM1 CAPACITE1 (Lattitude1, Longitude1) NOM2 CAPACITE2 (Lattitude2, Longitude2)
               Christ-Roi 100 (45.4977,-73.714) Elan 120 (45.4952,-73.6409) Face 95 (45.4468,-73.7152)
               
3.3 Sortie
Les résultats produits par votre programme doivent être écrits dans la sortie standard (stdout) à l'aide du flux de sortie C++ std::cout. Exemple de sortie :
Les personnes ainsi que les écoles doivent être traitées selon l'ordre de lecture du fichier (ne trier pas ni les personnes ni les écoles en ordre alphabétique). Pour chaque école on affiche :
 Première ligne : "Ecole : " le nom de la première école, suivi de ", Adresse : ", suivi de ces coordonnées.
 La deuxième ligne : "Capacité: " suivi de la valeur de capacité, suivi d’un nombre d’élèves inscrits.
 La troisième ligne et lignes suivantes éventuelles : lorsqu’il y a des inscriptions on voudra voir la liste d’élèves, le nom et le prénom par ligne. Lorsqu’il n’y a pas d’inscriptions on n’affiche ni la phrase « Liste d’élèves » ni les noms.
Vous devez impérativement respecter ce format (regardez l’image). Une simple erreur de frappe pourrait faire en sorte que votre programme échoue tous les tests de la correction automatisée.
 
3.4 Algorithmes
L'algorithme général peut ressembler à ceci.
Algorithme général :
Lire dans le tableau eleves
Lire dans le tableau ecoles
Pour el = 0 .. eleves.taille() – 1
| |Initialiser la distance MIN de départ | |Pour ec = .. ecoles.taille() - 1
| | Calculez la distance entre l’école et le lieu de résidence,
| | comparez la distance trouvée avec l’ancienne valeur de MIN
| | et vérifier si l’école accepte les inscriptions. Si oui, faites une
| | assignation, si non, passez à l’analyse de distance et de la
| | disponibilité des places dans la prochaine école.
Pour ec =0  .. ecoles.taille() - 1
|    Affichez ec;
4. Contraintes
4.1 Librairies permises
Vous devez implémenter et utiliser votre propre class générique Tableau selon le squelette dans tableau.h du laboratoire 3. Pour l'instant, l'utilisation des conteneurs de la librairie standard de C++ (ex.:std::vector ou std::list) n'est pas permise. Ce sera permis plus tard dans le cours.
4.2 Environnement de développement
Relisez les Politiques et les directives sur les outils informatiques dans le cours INF3105. Votre TP1 doit pouvoir être compilé avec g++ version 4.9 (version installé sur le serveur malt.labunix.uqam.ca.