/*    UQAM / Département d'informatique                          *
 *    INF3105 - Structures de données et algorithmes             *
 *    Automne 2015 / TP1                                         *
 *    AUTEUR(S): Euloge Nihezagire + NIHE16098601                *
 *               Henri-Joël HAZOUNME + HAZH12079009               */

#if !defined(__PERSONNE_H__)
#define __PERSONNE_H__

#include <iostream>
#include <string>
#include "coordonnee.h"
#include "tableau.h"

using namespace std;

class Personne {
public:
    string getNom() const;
    string getPrenom() const;
    Coordonnee getAdresse() const;
    bool operator==(const Personne& autre) const;


private:
    string nom;
    string prenom;
    Coordonnee coor;

    friend std::istream& operator>>(std::istream&, Personne&);
    friend std::ostream& operator<<(std::ostream &os, const Personne &p);
};

#endif
