#!/bin/bash
#set -v
#set -x

programme=tp1
ecoles=listeEcoles.txt

echo "===> rm $programme"
rm -f *.o *.obj *.ghc $programme

echo "===> make clean"
make clean
BASEDIR=$(dirname $0)
echo $BASEDIR
echo "===> make $programme"
make $programme
if [ $? -ne 0 ]
        then
        echo "  ERREUR : la commande 'make $programme' ne fonctionne pas!"
	exit $statut
fi

if [ ! -e $programme ]
	then
	echo "  ERREUR : l'exécutable $programme est inexistant!"
	exit -1
fi

if [ ! -x $programme ]
	then
	echo "  ERREUR : l'exécutable $programme n'est pas exécutable!"
	exit -1
fi

   
echo
echo "Vérification du bon fonctionnement de $programme avec quelques tests ..."

for essai in $(ls $BASEDIR/listeEleves??.txt)
do
    echo "=====> ./$programme $essai"

    ecole=$BASEDIR/listeEcoles${essai#$BASEDIR/listeEleves}

    ./$programme $essai $ecole > resultat
    echo "=====> Comparaison avec le résultat attentu :"
    
    diff -tibw resultat ${essai%.txt}.resultat
    if [ $? -eq 0 ]
    then
       echo " ===> OK <==="
    else
       echo " ===> ÉCHEC <==="
    fi
    
done


echo "Si vous êtes satisfait(e) des résultats, vous pouvez maintenant soumettre votre travail!"
echo "À noter que ces tests ne sont pas exhaustifs."
echo "========================== FIN ========================="

