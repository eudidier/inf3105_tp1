/*    UQAM / Département d'informatique                          *
 *    INF3105 - Structures de données et algorithmes             *
 *    Automne 2015 / TP1                                         *
 *    AUTEUR(S): Euloge Nihezagire + NIHE16098601                *
 *               Henri-Joël HAZOUNME + HAZH12079009              */

#include <algorithm>
#include "ecole.h"

Ecole::Ecole() {

}

Ecole::Ecole(int capaciteMax, string nom, Coordonnee c) {
    capacite = capaciteMax;
    nom = nom;
    adresse = c;
    Tableau<Personne> listeEleves(capacite);
}

Ecole::~Ecole() {
    capacite = 0;
    nom = "";
    listeEleves.~Tableau();
}

string Ecole::getNom() const {
    return this->nom;
}

Coordonnee Ecole::getAdresse() const {
    return this->adresse;
}

bool Ecole::plaine() const {
    bool retour = false;
    if (this->capacite == this->nombreInscrits()) {
        retour = true;
    }
    return retour;
}

int Ecole::nombreInscrits() const {
    return listeEleves.taille();
}

int Ecole::getCapacite() {
    return this->capacite;
}

Personne Ecole::getEleve(int i) {
    return listeEleves[i];
}

bool Ecole::inscrire(const Personne &p) {
    return listeEleves.ajouter(p);
}

bool Ecole::desinscrire(const Personne &p) {
    return listeEleves.enlever(listeEleves.trouver(p));
}

std::istream& operator>>(std::istream& is, Ecole& ecole) {
    string nom;
    Coordonnee coor;
    is >> ecole.nom >> ecole.capacite;
    if (ecole.nom.empty() || is.eof()) return is;
    is >> ecole.adresse;
    return is;
}

std::ostream& operator<<(std::ostream& os, Ecole& e) {
    os << "Ecole:" << e.getNom() << ", Adresse:"
            << e.getAdresse() << endl;
    os << "Capacite:" << e.getCapacite() << " Nombre d'eleves:"
            << e.nombreInscrits() << endl;

    return os;
}
