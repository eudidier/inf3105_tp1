/*    UQAM / Département d'informatique                          *
 *    INF3105 - Structures de données et algorithmes             *
 *    Automne 2015 / TP1                                         *
 *    AUTEUR(S): Euloge Nihezagire + NIHE16098601                *
 *               Henri-Joël HAZOUNME + HAZH12079009              */

#if !defined(__ECOLE_H__)
#define __ECOLE_H__

#include <iostream>
#include <string>
#include "coordonnee.h"
#include "tableau.h"
#include "personne.h"


using namespace std;

class Ecole{
  public:
    Ecole();
    Ecole(int capaciteMax, string nom, Coordonnee c);
    ~Ecole();
    string getNom() const;
    Coordonnee getAdresse() const;
    bool plaine() const;
    int nombreInscrits() const;
    int getCapacite();
    Personne getEleve(int i);
    bool inscrire(const Personne& p);
    bool desinscrire(const Personne& p);

  private:
    string nom;
    Coordonnee adresse;
    int capacite;
    Tableau<Personne> listeEleves;

    friend std::ostream& operator << (std::ostream& , Ecole &);
    friend std::istream& operator >> (std::istream& , Ecole & );
};
#endif 

