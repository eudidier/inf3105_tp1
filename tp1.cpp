/*    UQAM / Département d'informatique                          *
 *    INF3105 - Structures de données et algorithmes             *
 *    Automne 2015 / TP1                                         *
 *    AUTEUR(S): Euloge Nihezagire + NIHE16098601                *
 *              Henri-Joël HAZOUNME + HAZH12079009               *
 *    Un programme C++ qui assigne les élèves à leur école,selon *
 *    les distances calculées et le nombre de places disponibles.*
 *    BONUS : L'ALGORITHME FAIT LA DESINCRIPTION.                */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include "tableau.h"
#include "personne.h"
#include "ecole.h"

/* Déclaration des focntions */

using namespace std;
void inscrire_eleve(Tableau<Ecole>& ecoles, Personne eleve);
Personne desinscrire_eleve(Ecole& ecole_proche, Personne eleve, double min,
        Tableau<Ecole>& ecoles, int t);

/* 
 * Le main lit dans les fichiers passés en paramètre et appelle les focntions 
 * pour inscrire et désincrire un élève et affiche le résultat sur la sortie 
 * standard. 
 */
int main(int argc, const char** argv) {
    Tableau<Personne> eleves;
    Tableau<Ecole> ecoles;
    ifstream fichierEleves(argv[1]);
    while (fichierEleves) {
        Personne nouvellePersonne;
        fichierEleves >> nouvellePersonne;
        if (fichierEleves.eof()) break;
        eleves.ajouter(nouvellePersonne);
    }
    ifstream fichierEcoles(argv[2]);
    while (fichierEcoles) {
        Ecole nouvelleEcole;
        fichierEcoles >> nouvelleEcole;
        if (fichierEcoles.eof()) break;
        ecoles.ajouter(nouvelleEcole);
    }

    /*
     * Boucle qui inscrit les élèves et vérifie si toutes les écoles ne sont pas
     * remplies et retourne un message si un élève n'a pas pu être inscrit.
     * position : l'indice de l'école dans le teableau ecoles.
     * cpt : compte les écoles remplies.
     */
    for (int i = 0; i < eleves.taille(); i++) {

        int position = 0, cpt = 0;
        while (ecoles.taille() > position) {
            if (ecoles[position++].plaine()) {
                cpt++;
            }
        }
        if (cpt == position) {
            cout << "Il ne reste plus de place l'élève  " << eleves[i].getNom()
                    << " n'a pas pu être inscrit " << endl;
            break;
        }

        inscrire_eleve(ecoles, eleves[i]);
    }

    /*
     * Boucle qui effiche le resultat sur la sortie standard.
     */
    for (int i = 0; i < ecoles.taille(); i++) {

        cout << ecoles[i];

        if (ecoles[i].nombreInscrits() != 0) {

            cout << "Liste d'eleves: " << endl;

            for (int k = 0; k < ecoles[i].nombreInscrits(); k++) {

                cout << ecoles[i].getEleve(k);
            }
        }
    }

    ecoles.~Tableau();
    eleves.~Tableau();

    return 0;
}

/*
 * Fonction qui inscrit un élève et désinscrit un élève B si l'école est remplie 
 * et que l'élève A (à inscrire) et plus proche de l'école que B. 
 * Il réinscrit B recursivement.
 */
void inscrire_eleve(Tableau<Ecole>& ecoles, Personne eleve) {

    Personne eleves_desinscrit = eleve;
    double distance_min = ecoles[0].getAdresse().distance(eleve.getAdresse());

    int indice_ecole_proche = 0;

    for (int i = 0; i < ecoles.taille(); i++) {

        if (distance_min > ecoles[i].getAdresse().distance(eleve.getAdresse())) {

            distance_min = ecoles[i].getAdresse().distance(eleve.getAdresse());
            indice_ecole_proche = i;
        }
    }
    /* 
     * Pour enlever la fonction de désinscription il faut mettre en commentaire
     * les lignes 121 et 122.
     */
    if (ecoles[indice_ecole_proche].plaine()) {

        eleves_desinscrit = desinscrire_eleve(ecoles[indice_ecole_proche], eleve,
                distance_min, ecoles, indice_ecole_proche);

        if (eleves_desinscrit.getNom() != "") {

            Ecole ecoles_enlever = ecoles[indice_ecole_proche];

            ecoles.enlever(indice_ecole_proche);

            inscrire_eleve(ecoles, eleves_desinscrit);

            ecoles.inserer(ecoles_enlever, indice_ecole_proche);
        }
    }

    if (!ecoles[indice_ecole_proche].plaine() && eleves_desinscrit.getNom() != "") {

        ecoles[indice_ecole_proche].inscrire(eleve);

    }

}

/*
 * Fonction qui désinscrit un élève, le retourne pour l'inscrire dans une autre
 * école. indice est l'indice de l'école remplie.
 */
Personne desinscrire_eleve(Ecole& ecole_proche, Personne eleve, double min,
        Tableau<Ecole>& ecoles, int indice) {

    Personne eleves_desinscrit;
    Ecole ecole_enlever;
    double distance_max = -1; // -1 car min peut être égale à 0

    if (ecole_proche.nombreInscrits() != 0) {

        distance_max = ecole_proche.getAdresse().
                distance(ecole_proche.getEleve(0).getAdresse());

        int indice_ecole_proche = 0;

        for (int i = 0; i < ecole_proche.nombreInscrits(); i++) {

            if (distance_max <= ecole_proche.getAdresse().
                    distance(ecole_proche.getEleve(i).getAdresse())) {

                distance_max = ecole_proche.getAdresse().
                        distance(ecole_proche.getEleve(i).getAdresse());

                indice_ecole_proche = i;

            }
        }

        if (distance_max > min && ecole_proche.nombreInscrits() >= 1) {

            eleves_desinscrit = ecole_proche.getEleve(indice_ecole_proche);

            ecole_proche.
                    desinscrire(ecole_proche.getEleve(indice_ecole_proche));

            ecole_proche.inscrire(eleve);

        } else
            if (distance_max < min && ecole_proche.nombreInscrits() >= 1) {

            ecole_enlever = ecoles[indice];

            ecoles.enlever(indice);

            inscrire_eleve(ecoles, eleve);

            ecoles.inserer(ecole_enlever, indice);
        }
    }

    if (ecole_proche.nombreInscrits() == 0 && distance_max == -1) {

        ecole_enlever = ecoles[indice];

        ecoles.enlever(indice);

        inscrire_eleve(ecoles, eleve);

        ecoles.inserer(ecole_enlever, indice);
    }

    return eleves_desinscrit;
}