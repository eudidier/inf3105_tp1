/*    UQAM / Département d'informatique                          *
 *    INF3105 - Structures de données et algorithmes             *
 *    Automne 2015 / TP1                                         *
 *    AUTEUR(S): Euloge Nihezagire + NIHE16098601                *
 *               Henri-Joël HAZOUNME + HAZH12079009              */

#include <algorithm>
#include "personne.h"

string Personne::getNom() const {

    return this->nom;
}

string Personne::getPrenom() const {

    return this->prenom;
}

Coordonnee Personne::getAdresse() const {
    return this->coor;
}

bool Personne::operator==(const Personne &autre) const {
    bool retour = false;
    if (nom == autre.nom && prenom == autre.prenom)
        retour = true;
    return retour;
}

std::ostream& operator<<(std::ostream &os, const Personne &p) {
    os << p.getPrenom() << " " << p.getNom() << endl;
    return os;
}

std::istream& operator>>(std::istream& is, Personne& p) {

    string prenom;
    Coordonnee coor;
    char c;
    is >> p.nom;
    is >> p.prenom;
    if (p.nom.empty() || p.prenom.empty() || is.eof()) return is;
    is >> p.coor >> c;
    assert(c == ';');
    return is;
}

